'use strict';

///////////////////////////////////////////////////////////////////////////////
//
// WebSocket Signaling Channel
//
///////////////////////////////////////////////////////////////////////////////
/* globals MediaRecorder */

let recordedBlobs;
let mediaRecorder;
let mediaStreamObj;

function WebSocketSignalingChannel(connectButton, disconnectButton, remoteVideo, recordedVideo, recordButton, playButton, downloadButton, wsPath) {
    this.connectButton_ = connectButton;
    this.disconnectButton_ = disconnectButton;
    this.remoteVideo_ = remoteVideo;
    this.recordedVideo_ = recordedVideo;
    this.recordButton_ = recordButton;
    this.playButton_ = playButton;
    this.downloadButton_ = downloadButton;
    this.wsPath_ = wsPath;

    console.log('wsPath:' + wsPath);

    this.recordedVideo_.hidden = true;

    this.connectButton_.disabled = false;
    this.disconnectButton_.disabled = true;

    this.connectButton_.onclick = this.doSignalingConnect.bind(this);
    this.disconnectButton_.onclick = this.doSignalingDisconnnect.bind(this);

    // Take care of record button
    recordButton.addEventListener('click', () => {
        if (recordButton.textContent === 'Start Recording') {
            startRecording();
        } else {
            stopRecording();
            recordButton.textContent = 'Start Recording';
            playButton.disabled = false;
            downloadButton.disabled = false;
        }
    });

    // Take care of play button
    playButton.addEventListener('click', () => {
        // Show the recorded video element
        recordedVideo.hidden = false;

        const superBuffer = new Blob(recordedBlobs, {type: 'video/webm'});
        recordedVideo.src = null;
        recordedVideo.srcObject = null;
        recordedVideo.src = window.URL.createObjectURL(superBuffer);
        recordedVideo.controls = true;
        recordedVideo.play();
    });


    // Take care of recording display player
    downloadButton.addEventListener('click', () => {
        const blob = new Blob(recordedBlobs, {type: 'video/webm'});
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        a.download = 'test.webm';
        document.body.appendChild(a);
        a.click();
        setTimeout(() => {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 100);
    });


    window.onbeforeunload = this.doSignalingDisconnnect.bind(this);
};

function startRecording() {

    console.log('start recording')
    recordedBlobs = [];
    let options = {mimeType: 'video/webm;codecs=vp9,opus'};
    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
        console.error(`${options.mimeType} is not supported`);
        options = {mimeType: 'video/webm;codecs=vp8,opus'};
        if (!MediaRecorder.isTypeSupported(options.mimeType)) {
            console.error(`${options.mimeType} is not supported`);
            options = {mimeType: 'video/webm'};
            if (!MediaRecorder.isTypeSupported(options.mimeType)) {
                console.error(`${options.mimeType} is not supported`);
                options = {mimeType: ''};
            }
        }
    }
    try {
        ///THIS
        mediaRecorder = new MediaRecorder(mediaStreamObj, options);
    } catch (e) {
        console.error('Exception while creating MediaRecorder:', e);
        console.log(`Exception while creating MediaRecorder: ${JSON.stringify(e)}`);
        return;
    }

    console.log('Created MediaRecorder', mediaRecorder, 'with options', options);
    recordButton.textContent = 'Stop Recording';
    playButton.disabled = true;
    downloadButton.disabled = true;
    mediaRecorder.onstop = (event) => {
        console.log('Recorder stopped: ', event);
        console.log('Recorded Blobs: ', recordedBlobs);
    };
    mediaRecorder.ondataavailable = handleDataAvailable;
    mediaRecorder.start();
    console.log('MediaRecorder started', mediaRecorder);
}

function stopRecording() {
    mediaRecorder.stop();
}

function handleDataAvailable(event) {
    console.log('handleDataAvailable', event);
    if (event.data && event.data.size > 0) {
        recordedBlobs.push(event.data);
    }
}

WebSocketSignalingChannel.prototype.connectWebSocket = function () {
    var websocket_url;
    var websocket_url_path = this.wsPath_ || "/rws/ws";
    if (this.isPrivateIPaddress_(location.host))
        websocket_url = "ws://" + location.host + websocket_url_path;
    else
        websocket_url = "wss://" + location.host + websocket_url_path;
    trace("WebSocket URL : " + websocket_url);
    console.log(websocket_url);

    this.websocket_ = new WebSocket(websocket_url);
    this.websocket_.onopen = this.onWebSocketOpen_.bind(this);
    this.websocket_.onclose = this.onWebSocketClose_.bind(this);
    this.websocket_.onmessage = this.onWebSocketMessage_.bind(this);
    this.websocket_.onerror = this.onWebSocketError_.bind(this);
};

WebSocketSignalingChannel.prototype.onWebSocketOpen_ = function (event) {
    trace("Websocket connnected: " + this.websocket_.url);
    this.doSignalingRegister();
};

WebSocketSignalingChannel.prototype.onWebSocketClose_ = function (event) {
    trace("Websocket Disconnected");
    this.doSignalingDisconnnect();
    this.peerConnectionClient_.close();
    this.peerConnectionClient_ = null;
};

WebSocketSignalingChannel.prototype.onWebSocketMessage_ = function (event) {
    trace("WSS -> C: " + event.data);

    var dataJson = JSON.parse(event.data);
    if (dataJson["cmd"] == "send") {
        this.peerConnectionClient_.onReceivePeerMessage(dataJson["msg"]);
    }
};

WebSocketSignalingChannel.prototype.onWebSocketError_ = function (event) {
    trace("An error occured while connecting : " + event.data);
    // TODO: need error handling
};

WebSocketSignalingChannel.prototype.WebSocketSendMessage = function (message) {
    if (this.websocket_.readyState == WebSocket.OPEN ||
        this.websocket_.readyState == WebSocket.CONNECTING) {
        trace("C --> WSS: " + message);
        this.websocket_.send(message);
        return true;
    }
    trace("failed to send websocket message :" + message);
    return new Error('failed to send websocket message');
};


WebSocketSignalingChannel.prototype.doSignalingConnect = function () {
    // check whether WebSocket is suppored in this browser.
    if (window.WebSocket) {
        // supported
        this.connectButton_.disabled = true;
        this.disconnectButton_.disabled = false;
        this.recordButton_.disabled = false;
        this.peerConnectionClient_ =
            new PeerConnectionClient(this.remoteVideo_, this.doSignalingSend.bind(this), this.doEnableRecording.bind(this));
        this.connectWebSocket();
    } else {
        trace("doSignalingConnect: WebSocket is not suppported in this platform!");
        // WebSocket is not supported
        this.connectButton_.disabled = true;
        this.disconnectButton_.disabled = true;
        this.recordButton_.disabled = true;
    }
};

WebSocketSignalingChannel.prototype.doEnableRecording = function (data) {
    console.log('Got media', data)
    mediaStreamObj = data[0];
};


//
WebSocketSignalingChannel.prototype.doSignalingRegister = function () {
    // No Room concept, random generate room and client id.
    var register = {
        cmd: 'register',
        roomid: this.randomString_(9),
        clientid: this.randomString_(8)
    };
    var register_message = JSON.stringify(register);
    this.WebSocketSendMessage(register_message);
};

WebSocketSignalingChannel.prototype.doSignalingSend = function (data) {
    console.log(data);
    var message = {
        cmd: "send",
        msg: JSON.stringify(data),
        error: ""
    };
    var data_message = JSON.stringify(message);
    if (this.WebSocketSendMessage(data_message) == false) {
        trace("Failed to send data: " + data_message);
        return false;
    }
    ;
    return true;
};

WebSocketSignalingChannel.prototype.doSignalingDisconnnect = function () {
    this.connectButton_.disabled = false;
    this.disconnectButton_.disabled = true;
    this.recordButton_.disabled = true;
    if (this.websocket_.readyState == 1) {
        this.websocket_.close();
    }
    ;
};

///////////////////////////////////////////////////////////////////////////////
//
// Utility Helper functions
//
///////////////////////////////////////////////////////////////////////////////

WebSocketSignalingChannel.prototype.isPrivateIPaddress_ = function (ipaddress) {
    var parts = ipaddress.split('.');
    return parts[0] === '10' ||
        (parts[0] === '172' && (parseInt(parts[1], 10) >= 16 && parseInt(parts[1], 10) <= 31)) ||
        (parts[0] === '192' && parts[1] === '168');
};

WebSocketSignalingChannel.prototype.randomString_ = function (length) {
// Return a random numerical string.
    var result = [];
    var strLength = length || 5;
    var charSet = '0123456789';
    while (strLength--) {
        result.push(charSet.charAt(Math.floor(Math.random() * charSet.length)));
    }
    return result.join('');
};


